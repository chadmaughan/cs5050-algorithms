package assignment4;

import java.util.ArrayList;
import java.util.List;

public class BoyerMoore {
	
	private String pattern;
	private List<Integer> matches;
	
	public static void main(String[] args) {
	
		// just for testing (example from book)
		long start = System.currentTimeMillis();
		new BoyerMoore("abacab", "abacaabaccabacabaabb");
		long stop = System.currentTimeMillis();
		System.out.println("time: " + (stop - start));
	}

	public BoyerMoore(String pattern, String text) {
		
		this.pattern = pattern;
		this.matches = match(text);
	}
	
	private List<Integer> match(String text) {

		List<Integer> matches = new ArrayList<Integer>();

		int i = pattern.length() - 1;
		int j = pattern.length() - 1;
		
		do {
			if(pattern.charAt(j) == text.charAt(i)) {
				if(j == 0) {
					matches.add(i);
					i = i + pattern.length();
					j = pattern.length() - 1;
				}

				i--;
				j--;
			}
			else {
				i = i + pattern.length() - Math.min(j, (1 + last(text.charAt(i))));
				j = pattern.length() - 1;
			}
		}
		while ( i <= (text.length() - 1) );
		
		return matches;
	}
	
	private int last(char c) {

		int result = 0;
		for(int i = (pattern.length() - 1); i >= 0; i--) {
			if(c == pattern.charAt(i)) {
				result = i;
				break;
			}
		}

		return result;
	}

	public List<Integer> getMatches() {
		return matches;
	}
}
