package assignment4;

import java.util.ArrayList;
import java.util.List;

public class KMP {

	private List<Integer> matches;

	public static void main(String[] args) {
		
		// just for testing (example from book)
		long start = System.currentTimeMillis();
		new KMP("abacab", "abacaabaccabacabaabb");
		long stop = System.currentTimeMillis();
		System.out.println("time: " + (stop - start));
	}
	
	public KMP(String pattern, String text) {
		
		this.matches = match(pattern, text);		
	}
	
	public List<Integer> match(String pattern, String text) {

		List<Integer> matches = new ArrayList<Integer>();

		int[] failure = calculateFailureTable(pattern, text);
		
		int i = 0;
		int j = 0;
		
		while( (i < text.length()) && (j < pattern.length()) ) {
			if(pattern.charAt(j) == text.charAt(i)) {
				if(j == pattern.length() - 1) {
					int result = i - pattern.length() + 1;
					matches.add(result);
				}
				
				i++;
				j++;
			}
			else if(j > 0) {
				j = failure[j - 1];
			}
			else {
				i++;
			}
		}
		
		return matches;
	}
	
	public int[] calculateFailureTable(String pattern, String text) {

		int i = 2;
		int j = 0;

		int[] failure = new int[pattern.length()];
		failure[0] = 0;
		failure[1] = 0;
		
		while(i < pattern.length()) {
			if(pattern.charAt(j) == pattern.charAt(i)) {
				failure[i] = j + 1;
				i++;
				j++;
			}
			else if (j > 0) {
				j = failure[j - 1];
			}
			else {
				failure[i] = 0;
				i++;
			}
		}
		
		return failure;
	}
	
	public List<Integer> getMatches() {
		return matches;
	}

}
