package assignment4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Run {

	public static String nl = System.getProperty("line.separator");
	
	public static void main(String[] args) {

		try {
			
			File appBase = new File("src/assignment4");
			String path = appBase.getAbsolutePath();
			
			// get the DNA text
			BufferedReader dnaIn = new BufferedReader(new FileReader(path + "/DNA.txt"));
			StringBuffer dna = new StringBuffer();
			String strDna = null;
			while ((strDna = dnaIn.readLine()) != null) {
				dna.append(strDna);
			}
			dnaIn.close();
						
			
			// get Darwin's text
			BufferedReader textIn = new BufferedReader(new FileReader(path + "/TEXT.txt"));
			StringBuffer text = new StringBuffer();
			String strText = null;
			while ((strText = textIn.readLine()) != null) {
				text.append(strText);
			}
			textIn.close();

			// output the data to a file (for gnuplot)
			BufferedWriter dnaGrowingOutFile = new BufferedWriter(new FileWriter("/tmp/growing-dna-a4-data.txt")); 
			BufferedWriter dnaShrinkingOutFile = new BufferedWriter(new FileWriter("/tmp/shrinking-dna-a4-data.txt")); 

			BufferedWriter textGrowingOutFile = new BufferedWriter(new FileWriter("/tmp/growing-text-a4-data.txt")); 
			BufferedWriter textShrinkingOutFile = new BufferedWriter(new FileWriter("/tmp/shrinking-text-a4-data.txt")); 

			Random random = new Random();

			// run the different algorithms on various sizes of the supplied text
			// to see how they perform with different size of text to search
			for(int n = 1; n <= 32; n=(n-1)+2) {

				long start = 0l;
				long stop = 0l;

				System.out.println("--------------------------------------");
				System.out.println("Dividing supplied text length by: " + n);
			
				// shorten DNA string length by n
				System.out.println("Shortened dna to: " + (dna.length() / n));
				String sDna = dna.substring(0, (dna.length() / n));

				// shorten text string length by n
				System.out.println("Shortened text to: " + (text.length() / n));
				String sText = text.substring(0, (text.length() / n));

				int min = Math.min(sDna.length(), sText.length());
				int index = random.nextInt(min);
				int growingLength = (2 * n) + 10;
				if((index + growingLength) > min) {
					growingLength = min - index;
				}
				
				int shrinkingLength = (128 / n) + 10;
				if((index + shrinkingLength) > min) {
					shrinkingLength = min - index;
				}
				
				System.out.println("index: " + index);
				System.out.println("growing length: " + growingLength);
				System.out.println("shrinking length: " + shrinkingLength);

				// get a random dna pattern from the shortened string
				String growingDnaPattern = sDna.substring(index, (index + growingLength));
				String shrinkingDnaPattern = sDna.substring(index, (index + shrinkingLength));
				
				// get a random text pattern from the shortened string
				String growingTextPattern = sText.substring(index, (index + growingLength));
				String shrinkingTextPattern = sText.substring(index, (index + shrinkingLength));

				// run the different algorithms on DNA (length / n) - growing
				System.out.println("selected random (growing) dna pattern: " + growingDnaPattern);

				StringBuffer dnaGrowingOut = new StringBuffer();
				dnaGrowingOut.append(n + "\t" + growingLength + "\t");
				
				start = System.currentTimeMillis();
				new BruteForce(growingDnaPattern, sDna);
				stop = System.currentTimeMillis();
				dnaGrowingOut.append((stop - start) + "\t"); 
				System.out.println("dna brute force time: " + (stop - start));

				start = System.currentTimeMillis();
				new BoyerMoore(growingDnaPattern, sDna);
				stop = System.currentTimeMillis();
				dnaGrowingOut.append((stop - start) + "\t"); 
				System.out.println("dna boyer-moore time: " + (stop - start));

				start = System.currentTimeMillis();
				new KMP(growingDnaPattern, sDna);
				stop = System.currentTimeMillis();
				dnaGrowingOut.append((stop - start) + "\t"); 
				System.out.println("dna kmp time: " + (stop - start));

				dnaGrowingOutFile.write(dnaGrowingOut.toString() + nl);
				
				// run the different algorithms on DNA (length / n) - shrinking
				System.out.println("selected random dna (shrinking) pattern: " + shrinkingDnaPattern);

				StringBuffer dnaShrinkingOut = new StringBuffer();
				dnaShrinkingOut.append(n + "\t" + shrinkingLength + "\t");

				start = System.currentTimeMillis();
				new BruteForce(shrinkingDnaPattern, sDna);
				stop = System.currentTimeMillis();
				dnaShrinkingOut.append((stop - start) + "\t"); 
				System.out.println("dna brute force time: " + (stop - start));

				start = System.currentTimeMillis();
				new BoyerMoore(shrinkingDnaPattern, sDna);
				stop = System.currentTimeMillis();
				dnaShrinkingOut.append((stop - start) + "\t"); 
				System.out.println("dna boyer-moore time: " + (stop - start));

				start = System.currentTimeMillis();
				new KMP(shrinkingDnaPattern, sDna);
				stop = System.currentTimeMillis();
				dnaShrinkingOut.append((stop - start) + "\t"); 
				System.out.println("dna kmp time: " + (stop - start));

				dnaShrinkingOutFile.write(dnaShrinkingOut.toString() + nl);

				
				// run the different algorithms on Darwin's (length / i) - growing
				System.out.println("selected random text (growing) pattern: " + growingTextPattern);

				StringBuffer textGrowingOut = new StringBuffer();
				textGrowingOut.append(n + "\t" + growingLength + "\t");

				start = System.currentTimeMillis();
				new BruteForce(growingTextPattern, sText);
				stop = System.currentTimeMillis();
				textGrowingOut.append((stop - start) + "\t"); 
				System.out.println("text brute force time: " + (stop - start));

				start = System.currentTimeMillis();
				new BoyerMoore(growingTextPattern, sText);
				stop = System.currentTimeMillis();
				textGrowingOut.append((stop - start) + "\t"); 
				System.out.println("text boyer-moore time: " + (stop - start));

				start = System.currentTimeMillis();
				new KMP(growingTextPattern, sText);
				stop = System.currentTimeMillis();
				textGrowingOut.append((stop - start) + "\t"); 
				System.out.println("text kmp time: " + (stop - start));

				textGrowingOutFile.write(textGrowingOut.toString() + nl);

				// run the different algorithms on Darwin's (length / i) - shrinking
				System.out.println("selected random text (shrinking) pattern: " + shrinkingTextPattern);

				StringBuffer textShrinkingOut = new StringBuffer();
				textShrinkingOut.append(n + "\t" + shrinkingLength + "\t");

				start = System.currentTimeMillis();
				new BruteForce(shrinkingTextPattern, sText);
				stop = System.currentTimeMillis();
				textShrinkingOut.append((stop - start) + "\t"); 
				System.out.println("text brute force time: " + (stop - start));

				start = System.currentTimeMillis();
				new BoyerMoore(shrinkingTextPattern, sText);
				stop = System.currentTimeMillis();
				textShrinkingOut.append((stop - start) + "\t"); 
				System.out.println("text boyer-moore time: " + (stop - start));

				start = System.currentTimeMillis();
				new KMP(shrinkingTextPattern, sText);
				stop = System.currentTimeMillis();
				textShrinkingOut.append((stop - start) + "\t"); 
				System.out.println("text kmp time: " + (stop - start));

				textShrinkingOutFile.write(textShrinkingOut.toString() + nl);

			}

			dnaShrinkingOutFile.close();
			dnaGrowingOutFile.close();
			
			textShrinkingOutFile.close();
			textGrowingOutFile.close();
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
