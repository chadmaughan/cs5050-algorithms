package assignment4;

import java.util.ArrayList;
import java.util.List;

public class BruteForce {

	private List<Integer> matches;

	public static void main(String[] args) {

		// just for testing (example from book)
		long start = System.currentTimeMillis();
		new BruteForce("abacab", "abacaabaccabacabaabb");
		long stop = System.currentTimeMillis();
		System.out.println("time: " + (stop - start));
	}

	public BruteForce(String pattern, String text) {

		this.matches = match(pattern, text);		
	}
	
	private List<Integer> match(String pattern, String text) {

		List<Integer> matches = new ArrayList<Integer>();		

		for(int i = 0; i < (text.length() - pattern.length()); i++) {
			
			int j = 0;

			while (j < pattern.length() && (text.charAt(i + j) == pattern.charAt(j))) {
				j++;
			}
			
			if(j == pattern.length()) {
				matches.add(i);
			}
		}
		
		return matches;
	}
	
	public List<Integer> getMatches() {
		return matches;
	}

}
