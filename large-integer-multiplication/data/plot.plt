reset
set term png font Arial 10 crop
set size 1.0, 0.8
set xlabel "problem size"
set ylabel "cpu run time (s)"
set xrange[2 : 1024]
set yrange[2 : 1100000]
set log
set nogrid
set key left
set output "plot.png"
plot 'data.txt' using 1:2 title "simple algorithm" with linesp 1, 'data.txt' using 1:3 title "complex algorithm" with linesp 3 

