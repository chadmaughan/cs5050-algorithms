package fft;

import java.util.ArrayList;
import java.util.List;

public class RecursiveFFT {

	private List<Complex> answer;
	
	public static void main(String[] args) {

		// test data from Dr. Flann's example (bb.usu.edu)
		List<Complex> p = new ArrayList<Complex>();
		p.add(new Complex(0, 0));
		p.add(new Complex(1, 0));
		p.add(new Complex(2, 0));
		p.add(new Complex(3, 0));
		p.add(new Complex(0, 0));
		p.add(new Complex(0, 0));
		p.add(new Complex(0, 0));
		p.add(new Complex(0, 0));

		List<Complex> q = new ArrayList<Complex>();
		q.add(new Complex(10, 0));
		q.add(new Complex(11, 0));
		q.add(new Complex(12, 0));
		q.add(new Complex(13, 0));
		q.add(new Complex(0, 0));
		q.add(new Complex(0, 0));
		q.add(new Complex(0, 0));
		q.add(new Complex(0, 0));

		RecursiveFFT rfft = new RecursiveFFT(p, q);
		List<Complex> answer = rfft.getAnswer();

		for(Complex c : answer) {
			System.out.println(c.toString());
		}		
	}

	public RecursiveFFT(List<Complex> p, List<Complex> q) {

		Complex pOmega = new Complex(p.size());
		List<Complex> pAnswer = this.fft(p, pOmega, p.size());

		Complex qOmega = new Complex(q.size());
		List<Complex> qAnswer = this.fft(q, qOmega, q.size());
		
		List<Complex> combined = this.multiply(pAnswer, qAnswer);
		this.answer = this.fft(combined, new Complex(combined.size()), combined.size());
	}

	public List<Complex> multiply(List<Complex> p, List<Complex> q) {

		ArrayList<Complex> c = new ArrayList<Complex>();
		for (int i = 0; i < p.size(); i++)
			c.add(new Complex(0, 0));

		for(int i = 0; i < p.size(); i++) {
			Complex ai = p.get(i).multiply(q.get(i));
			c.set(i, ai);
		}
		
		return c;
	}
	
	public List<Complex> fft(List<Complex> data, Complex omega, int n) {

		// base case
		if (n == 1)
			return data;

		// split
		List<Complex> even = new ArrayList<Complex>();
		List<Complex> odd = new ArrayList<Complex>();
		for (int i = 0; i < n; i = i + 2) {
			even.add(data.get(i));
			odd.add(data.get(i + 1));
		}

		// recursion
		List<Complex> rEven = fft(even, omega.multiply(omega), n / 2);
		List<Complex> rOdd = fft(odd, omega.multiply(omega), n / 2);

		// answer array
		List<Complex> answer = new ArrayList<Complex>();
		for (int i = 0; i < n; i++)
			answer.add(new Complex(0, 0));

		// combine
		Complex x = new Complex(1, 0);
		for (int i = 0; i < n / 2; i++) {
			answer.set(i, rEven.get(i).add(x.multiply(rOdd.get(i))));
			answer.set(i + n / 2, rEven.get(i).subtract(x.multiply(rOdd.get(i))));
			x = x.multiply(omega);
		}
		return answer;
	}

	public List<Complex> getAnswer() {
		return answer;
	}

	public void setAnswer(List<Complex> answer) {
		this.answer = answer;
	}
}