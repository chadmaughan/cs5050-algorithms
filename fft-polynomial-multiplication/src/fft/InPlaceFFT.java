package fft;

import java.util.ArrayList;
import java.util.List;

public class InPlaceFFT {

	private Roots roots;
	private List<Complex> answer;
	
	public static void main(String[] args) {
		
		// test data from Dr. Flann's example (bb.usu.edu)
		List<Complex> p = new ArrayList<Complex>();
		p.add(new Complex(0, 0));
		p.add(new Complex(1, 0));
		p.add(new Complex(2, 0));
		p.add(new Complex(3, 0));
		p.add(new Complex(0, 0));
		p.add(new Complex(0, 0));
		p.add(new Complex(0, 0));
		p.add(new Complex(0, 0));
		
		List<Complex> q = new ArrayList<Complex>();
		q.add(new Complex(10, 0));
		q.add(new Complex(11, 0));
		q.add(new Complex(12, 0));
		q.add(new Complex(13, 0));
		q.add(new Complex(0, 0));
		q.add(new Complex(0, 0));
		q.add(new Complex(0, 0));
		q.add(new Complex(0, 0));

		InPlaceFFT ipfft = new InPlaceFFT(p, q, p.size());
		List<Complex> answer = ipfft.getAnswer();
		for(Complex c : answer) {
			System.out.println(c.toString());
		}
	}

	public InPlaceFFT(List<Complex> p, List<Complex> q, int n) {
		
		this.roots = new Roots();
		this.roots.calculate(n);
		
		List<Complex> pAnswer = this.fft(p);
		List<Complex> qAnswer = this.fft(q);
		
		List<Complex> combined = this.multiply(pAnswer, qAnswer);
		this.answer = this.fft(combined);
	}

	public List<Complex> multiply(List<Complex> p, List<Complex> q) {

		ArrayList<Complex> c = new ArrayList<Complex>();
		for (int i = 0; i < p.size(); i++)
			c.add(new Complex(0, 0));

		for(int i = 0; i < p.size(); i++) {
			Complex ai = p.get(i).multiply(q.get(i));
			c.set(i, ai);
		}
		
		return c;
	}

	public List<Complex> fft(List<Complex> data) {

		int n = data.size();

		// answer array
		ArrayList<Complex> answer = new ArrayList<Complex>();
		for (int i = 0; i < n; i++)
			answer.add(new Complex(0, 0));

		// base case
		if (n == 1) {
			answer.set(0, data.get(0));
			return answer;
		}

		// base case
		if (n == 2) {
			answer.set(0, data.get(0).add(data.get(1)));
			answer.set(0, data.get(0).subtract(data.get(1)));
			return answer;
		}

		// bit reversal
		int ii = 0;
		for (int i = 0; i < n; i++) {
			answer.set(i, data.get(ii));
			int k = n >> 1;
			while (ii >= k && k > 0) {
				ii -= k;
				k >>= 1;
			}
			ii += k;
		}

		// magic
		for (int i = 0; i < n; i += 4) {
			
			Complex a = answer.get(i).add(answer.get(i + 1));
			Complex b = answer.get(i + 2).add(answer.get(i + 3));
			Complex c = answer.get(i).subtract(answer.get(i + 1));
			Complex d = answer.get(i + 2).subtract(answer.get(i + 3));

			Complex e1 = c.add(d.multiply(new Complex(0.0, 1.0)));
			Complex e2 = c.subtract(d.multiply(new Complex(0.0, 1.0)));
			
			answer.set(i, a.add(b));
			answer.set(i + 2, a.subtract(b));

			answer.set(i + 1, e2);
			answer.set(i + 3, e1);
		}

		// "Any sufficiently advanced technology is indistinguishable from magic" - Arthur C. Clarke
		for (int i = 4; i < n; i <<= 1) {
			
			int m = n / (i << 1);
			for (int j = 0; j < n; j += i << 1) {
			
				for (int k = 0; k < i; k++) {

					int km = k * m;
					
					double omega_km_real = roots.getReal(km);
					double omega_km_imaginary = roots.getImaginary(km);

					Complex c = new Complex(
							answer.get(i + j + k).getReal() * omega_km_real - answer.get(i + j + k).getImaginary() * omega_km_imaginary, 
							answer.get(i + j + k).getReal() * omega_km_imaginary + answer.get(i + j + k).getImaginary() * omega_km_real);

					answer.set(i + j + k, answer.get(j + k).subtract(c));
					answer.set(j + k, answer.get(j + k).add(c));
				}
			}
		}
		return answer;
	}

	public Roots getRoots() {
		return roots;
	}

	public void setRoots(Roots roots) {
		this.roots = roots;
	}

	public List<Complex> getAnswer() {
		return answer;
	}

	public void setAnswer(List<Complex> answer) {
		this.answer = answer;
	}
}
