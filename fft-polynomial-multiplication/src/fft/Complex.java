package fft;

public class Complex {

	private double real;
	private double imaginary;

	public Complex(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	public Complex(int n) {
		real = Math.cos(2 * Math.PI / n);
		imaginary = Math.sin(2 * Math.PI / n);
	}

	public Complex add(Complex c) {
		return new Complex(real + c.real, imaginary + c.imaginary);
	}

	public Complex subtract(Complex c) {
		return new Complex(real - c.real, imaginary - c.imaginary);
	}

	public Complex multiply(Complex c) {
		return new Complex(
				(real * c.real - imaginary * c.imaginary), 
				(real * c.imaginary + imaginary	* c.real));
	}

	public double getReal() {
		return real;
	}

	public void setReal(double real) {
		this.real = real;
	}

	public double getImaginary() {
		return imaginary;
	}

	public void setImaginary(double imaginary) {
		this.imaginary = imaginary;
	}
	
	public String toString() {
//		return String.format("%8.4f  + %8.4fi", real, imaginary);
		return real + ", " + imaginary;
	}

}
