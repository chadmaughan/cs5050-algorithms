package fft;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import bigIntegerMultiplication.BigIntegerMultiplication;

public class Driver {

	/*
	 * Driver class for FFT implementations
	 */
	public static void main(String[] args) {

		try {

			FileWriter fstream = new FileWriter(System.getProperty("java.io.tmpdir") + "/fft-data.txt");
			BufferedWriter out = new BufferedWriter(fstream);

			for(int i = 3; i < 17; i++) {
	
				List<Long> pl = new ArrayList<Long>();
				List<Complex> p = new ArrayList<Complex>();
				for(int j = 0; j < Math.pow(2,i); j++) {
					int pi = (int) (Math.random() * 10);
					pl.add(new Long(pi));
					p.add(new Complex(pi, 0));
				}
				
				List<Long> ql = new ArrayList<Long>();
				List<Complex> q = new ArrayList<Complex>();
				for(int j = 0; j < Math.pow(2,i); j++) {
					int qi = (int) (Math.random() * 10);
					ql.add(new Long(qi));
					q.add(new Complex(qi, 0));
				}
	
				// big integer
				long bStart = System.currentTimeMillis();
				BigIntegerMultiplication.clever(pl, ql);
				long bStop = System.currentTimeMillis();
				System.out.println("bigint: " + (bStop - bStart));
	
				// recursive implementation
				long rStart = System.currentTimeMillis();
				RecursiveFFT rfft = new RecursiveFFT(p, q);
				rfft.getAnswer();
				long rStop = System.currentTimeMillis();
				System.out.println("recursive: " + (rStop - rStart));
				
				// DP implementation
				long dpStart = System.currentTimeMillis();
				InPlaceFFT ipfft = new InPlaceFFT(p, q, p.size());
				ipfft.getAnswer();
				long dpStop = System.currentTimeMillis();
				System.out.println("dp: " + (dpStop - dpStart));

				out.write(i + "\t" + (bStop - bStart) + "\t" + (rStop - rStart) + "\t" + (dpStop - dpStart) + "\n");

			}
			
			out.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}