import java.awt.Point;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Random;

import tsp.CleverSortTSP;
import tsp.CleverTSP;
import tsp.SimpleSortTSP;
import tsp.SimpleTSP;

public class Driver {

	public static void main(String[] args) {

		// how far apart should the cities be (analagously speaking, how big should the country be that we compute the cities in)
		int maxGraphDistance = 100;
		
		try {
			FileWriter fstream = new FileWriter("/tmp/tsp-data.txt");
			BufferedWriter out = new BufferedWriter(fstream);

			for (int i = 3; i < 14; i++) {

				long astspt = 0;
				long actspt = 0;
				long asstspt = 0;
				long acstspt = 0;

				// compute the same solution twice to average out unusually efficient tours
				for(int j = 1; j <= 3; j++) {
					
					int[][] costMatrix = generateCostMatrix(i, maxGraphDistance);
										
					SimpleTSP stsp = new SimpleTSP(costMatrix);
					stsp.printCostMatrix(costMatrix);
					stsp.generateSolution();
					long stspt = stsp.getElapsedTime();
					System.out.println("");
					System.out.println("       TIME: " + stsp.getElapsedTime());
					System.out.println("TOUR LENGTH: " + stsp.getOptimalTourLength());
					System.out.println("  TOUR PATH: " + stsp.printOptimalTour());
					
					CleverTSP ctsp = new CleverTSP(costMatrix);
					ctsp.printCostMatrix(costMatrix);
					ctsp.generateSolution();
					long ctspt = ctsp.getElapsedTime();
					System.out.println("");
					System.out.println("       TIME: " + ctsp.getElapsedTime());
					System.out.println("TOUR LENGTH: " + ctsp.getOptimalTourLength());
					System.out.println("  TOUR PATH: " + ctsp.printOptimalTour());
					
					SimpleSortTSP sstsp = new SimpleSortTSP(costMatrix);
					sstsp.printCostMatrix(costMatrix);
					sstsp.generateSolution();
					long sstspt = sstsp.getElapsedTime();
					System.out.println("");
					System.out.println("       TIME: " + sstsp.getElapsedTime());
					System.out.println("TOUR LENGTH: " + sstsp.getOptimalTourLength());
					System.out.println("  TOUR PATH: " + sstsp.printOptimalTour());
				
					CleverSortTSP cstsp = new CleverSortTSP(costMatrix);
					cstsp.printCostMatrix(costMatrix);
					cstsp.generateSolution();
					long cstspt = cstsp.getElapsedTime();
					System.out.println("");
					System.out.println("       TIME: " + cstsp.getElapsedTime());
					System.out.println("TOUR LENGTH: " + cstsp.getOptimalTourLength());
					System.out.println("  TOUR PATH: " + cstsp.printOptimalTour());
					
					astspt = (astspt + stspt) / j;
					actspt = (actspt + ctspt) / j;
					asstspt = (asstspt + sstspt) / j;
					acstspt = (acstspt + cstspt) / j;

				}
				
				out.write(i + "," + astspt + "," + actspt + "," + asstspt + "," + acstspt + System.getProperty("line.separator"));
			}
			
			// Close the output stream
			out.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	private static int[][] generateCostMatrix(int size, int maxDistance) {

		Random random = new Random();

		// create the cities
		Point[] cities = new Point[size];
		for(int i = 0; i < size; i++) {
			int x = random.nextInt(maxDistance-1) + 1;
			int y = random.nextInt(maxDistance-1) + 1;
			cities[i] = new Point(x,y);
		}

		// now create the adjacency (or cost) matrix
		// 		(use java.awt.Point to easily calculate Euclidean distance)
		int[][] cost = new int[size][size];
		
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				if(i == j) {
					cost[i][j] = 0;
				}
				else {
					// cast this as an int (equivalent of rounding down) to simplify distance calculations in algorithm
					cost[i][j] = (int) cities[i].distance(cities[j]);
				}
			}
		}

		return cost;
	}
}
