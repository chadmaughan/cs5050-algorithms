package tsp.nodes;


/*
 * A 'SimpleNode' represents a container in the state space tree
 * 
 * It uses a simple algorithm of distance traveled for computing a lower bound
 * and employs _no_ sorting heuristics
 */
public class SimpleNode extends Node implements Comparable<SimpleNode> {

	public SimpleNode(int[][] costMatrix) {
		super(costMatrix);
	}

	public int compareTo(SimpleNode node) {

		// first check is obvious, if they don't have the same number of cities, they're not the same
		if (this.cities.size() < node.getCities().size()) {
			// System.out.println("t.cities.len < n.cities.len: " + this.cities.size() + ", " + node.getCities().size());
			return 1;
		}
		else if (this.cities.size() > node.getCities().size()) {
			// System.out.println("t.cities.len > n.cities.len: " + this.cities.size() + ", " + node.getCities().size());
			return -1;
		}
		else if (this.cities.size() == node.getCities().size()) {

			if (this.getBound() < node.getBound()) {
				// System.out.println("t.bound < n.bound");
				return -1;
			}
			else if (this.getBound() > node.getBound()) {
				// System.out.println("t.bound > n.bound");
				return 1;
			}
			else if (this.getBound() == node.getBound()) {

				// Add up the sum of the cities
				int sumThis = 0;
				for (Integer i : this.cities) {
					sumThis += i;
				}

				int sumOther = 0;
				for (Integer i : node.getCities()) {
					sumOther += i;
				}

				if (sumThis == sumOther) {
					return 0;
				}
				else if (sumThis > sumOther) {
					// System.out.println("t.len > n.len");
					return 1;
				}
				else { // (sumThis < sumOther) {
					// System.out.println("t.len <= n.len");
					return -1;
				}
			}
		}
		return Integer.MAX_VALUE;
	}

	public boolean equals(SimpleNode node) {
		return this.compareTo(node) == 0;
	}

	public int getBound() {
		return super.getLength();
	}

}
