package tsp.nodes;

/*
 * A 'CleverNode' represents a container in the state space tree
 * 
 * It uses a 'promising' algorithm for computing a lower bound
 * and employs a 'greedy' sorting heuristics always taking the closest city
 * for graph expansion
 */
public class CleverSortNode extends Node implements Comparable<CleverSortNode> {

	public CleverSortNode(int[][] costMatrix) {
		super(costMatrix);
	}

	public int compareTo(CleverSortNode node) {

		// first check is obvious, if they don't have the same number of cities, they're not the same
		if (this.cities.size() < node.getCities().size()) {
			// System.out.println("t.cities.len < n.cities.len: " + this.cities.size() + ", " + node.getCities().size());
			return 1;
		}
		else if (this.cities.size() > node.getCities().size()) {
			// System.out.println("t.cities.len > n.cities.len: " + this.cities.size() + ", " + node.getCities().size());
			return -1;
		}
		else if (this.cities.size() == node.getCities().size()) {

			// 
			if (this.getBound() < node.getBound()) {
				// System.out.println("t.bound < n.bound");
				return -1;
			}
			else if (this.getBound() > node.getBound()) {
				// System.out.println("t.bound > n.bound");
				return 1;
			}
			else if (this.getBound() == node.getBound()) {

				// get the last city on the path
				int thisLastCity = this.cities.get(cities.size() - 1);
				int nodeLastCity = node.getCities().get(cities.size() - 1);

				// get the next closest city
				int thisSmallestCost = Integer.MAX_VALUE;
				for(int i = 0; i < this.size; i++) {
					thisSmallestCost = Math.min(thisSmallestCost, this.costMatrix[thisLastCity][i]);
				}

				// get the next closest city
				int nodeSmallestCost = Integer.MAX_VALUE;
				for(int i = 0; i < this.size; i++) {
					nodeSmallestCost = Math.min(nodeSmallestCost, this.costMatrix[nodeLastCity][i]);
				}

				if(thisSmallestCost < nodeSmallestCost) {
					return -1;
				}
				else if (thisSmallestCost > nodeSmallestCost) {
					return 1;
				}
				else { // thisSmallestCost == nodeSmallestCost
					return 0;
				}
			}
		}
		return Integer.MAX_VALUE;
	}

	public boolean equals(CleverSortNode node) {
		return this.compareTo(node) == 0;
	}

	public int getBound() {

		int bound = 0;

		if (cities.size() == 1) {
			for (int i = 0; i < this.size; i++) {
				bound += minimum(i);
			}
		}
		else {

			for (int i = 1; i < cities.size(); i++) {
				skip[cities.get(i - 1)] = true;
				bound += this.costMatrix[cities.get(i - 1)][cities.get(i)];
			}

			skip[0] = true;
			bound += minimum(cities.get(cities.size() - 1));

			skip[0] = false;
			skip[cities.get(cities.size() - 1)] = true;

			for (int i = 1; i < this.size; i++) {
				if (!skip[i]) {
					bound += minimum(i);
				}
			}
		}

		// System.out.println("bound (" + this.toString() + "): " + bound);
		return bound;
	}

}
