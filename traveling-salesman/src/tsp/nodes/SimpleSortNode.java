package tsp.nodes;

/*
 * A 'SimpleNode' represents a container in the state space tree
 * 
 * It uses a simple algorithm of distance traveled for computing a lower bound
 * and employs a 'greedy' sorting heuristics always taking the closest city
 * for graph expansion
 */
public class SimpleSortNode extends Node implements Comparable<SimpleSortNode> {

	public SimpleSortNode(int[][] costMatrix) {
		super(costMatrix);
	}

	public int compareTo(SimpleSortNode node) {

		if (this.cities.size() < node.getCities().size()) {
			// System.out.println("t.cities.len < n.cities.len: " + this.cities.size() + ", " + node.getCities().size());
			return 1;
		}
		else if (this.cities.size() > node.getCities().size()) {
			// System.out.println("t.cities.len > n.cities.len: " + this.cities.size() + ", " + node.getCities().size());
			return -1;
		}
		else if (this.cities.size() == node.getCities().size()) {

			if (this.getBound() < node.getBound()) {
				// System.out.println("t.bound < n.bound");
				return -1;
			}
			else if (this.getBound() > node.getBound()) {
				// System.out.println("t.bound > n.bound");
				return 1;
			}
			else if (this.getBound() == node.getBound()) {

				// get the last city on the path
				int thisLastCity = this.cities.get(cities.size() - 1);
				int nodeLastCity = node.getCities().get(cities.size() - 1);

				// get the next closest city
				int thisSmallestCost = Integer.MAX_VALUE;
				for(int i = 0; i < this.size; i++) {
					thisSmallestCost = Math.min(thisSmallestCost, this.costMatrix[thisLastCity][i]);
				}

				// get the next closest city
				int nodeSmallestCost = Integer.MAX_VALUE;
				for(int i = 0; i < this.size; i++) {
					nodeSmallestCost = Math.min(nodeSmallestCost, this.costMatrix[nodeLastCity][i]);
				}

				if(thisSmallestCost < nodeSmallestCost) {
					return -1;
				}
				else if (thisSmallestCost > nodeSmallestCost) {
					return 1;
				}
				else { // thisSmallestCost == nodeSmallestCost
					return 0;
				}
			}
		}
		return Integer.MAX_VALUE;
	}

	public boolean equals(SimpleSortNode node) {
		return this.compareTo(node) == 0;
	}

	public int getBound() {
		return super.getLength();
	}

}
