package assignment3.two;

public class KnapsackDynamic {

	private static int items = 5;

	private static int capacity1 = 5;
	private static int capacity2 = 15;

	private static int[] value = new int[items + 1];
	private static int[] weight = new int[items + 1];

	public static void main(String[] args) {

		//    item	value	weight	take
		//    1		603		0		true
		//    2		339		5		false
		//    3		693		6		true
		//    4		667		8		false
		//    5		179		6		false
        value[1] = 603;		weight[1] = 0;
        value[2] = 339;		weight[2] = 5;
        value[3] = 693;		weight[3] = 6;
        value[4] = 667;		weight[4] = 8;
        value[5] = 179;		weight[5] = 6;

		int[][][] options = new int[items + 1][capacity1 + 1][capacity2 + 1];
		boolean[][][] solutions = new boolean[items + 1][capacity1 + 1][capacity2 + 1];

		// create all the base cases (bottom up for dynamic programming
		// solution)
		for (int n = 1; n <= items; n++) {

			for (int w1 = 1; w1 <= capacity1; w1++) {

				for (int w2 = 1; w2 <= capacity2; w2++) {

					System.out.println("n: " + n + ", w1: " + w1 + ", w2: " + w2);

					// get the last option
					int option1 = options[n - 1][w1][w2];
					System.out.println("opt1: " + option1);

					int option2 = 0;
					if (weight[n] <= w1) {
						option2 = options[n - 1][w1 - weight[n]][w2] + value[n];
						System.out.println("opt2: " + option2);
					}

					int option3 = 0;
					if (weight[n] <= w2) {
						option3 = options[n - 1][w1][w2 - weight[n]] + value[n];
						System.out.println("opt3: " + option3);
					}

					// select better of two options
					options[n][w1][w2] = Math.max(option1, Math.max(option2, option3));
					solutions[n][w1][w2] = option1 < Math.max(option2, option3);
				}
			}
		}
		
		for (int n = 1; n <= items; n++) {
			for (int w1 = 1; w1 <= capacity1; w1++) {
				for (int w2 = 1; w2 <= capacity2; w2++) {
					System.out.println("options[" + n + "][" + w1 + "][" + w2 + "]: " + options[n][w1][w2]);
				}
			}
		}

        int result = 0;
		for (int n = 1; n <= items; n++) {
			for (int w1 = 1; w1 <= capacity1; w1++) {
				for (int w2 = 1; w2 <= capacity2; w2++) {
					System.out.println("solutions[" + n + "][" + w1 + "][" + w2 + "]: " + solutions[n][w1][w2]);
					if(solutions[n][w1][w2]) {
						System.out.println(options[n][w1][w2]);
						result += options[n][w1][w2];
					}
				}
			}
		}
		System.out.println(result);
	}
}
