package assignment3.two;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class KnapsackCombined extends Thread {

	private static int items = 100;
	private static int capacity1 = 10;
    private static int capacity2 = 10;

    private static int[] value = new int[items + 1];
    private static int[] weight = new int[items + 1];

    private static int[][][] cache = new int[items + 1][capacity1 + 1][capacity2 + 1];

    public static void main(String[] args) {

        // generate random values and weights for items, item weight should be positive and less than total weight
        for (int n = 1; n <= items; n++) {
            value[n] = (int) (Math.random() * 1000);
            weight[n] = (int) (Math.random() * capacity1); 
        }

        // initialize the cache
        for(int n = 0; n < items; n++) {
        	for(int w1 = 0; w1 < capacity1; w1++) {
        		for(int w2 = 0; w2 < capacity2; w2++) {
        			cache[n][w1][w2] = 0;
        		}
        	}
        }

		try {

			FileWriter fstream = new FileWriter("/tmp/two-data-100.txt", true);
			BufferedWriter out = new BufferedWriter(fstream);

			for (int i = 2; i <= 16; i += 2) {

				System.out.println("i: " + i);

			    value = new int[items + 1];
			    weight = new int[items + 1];

			    cache = new int[items + 1][capacity1 + 1][capacity2 + 1];

		        // generate random values and weights for items, item weight should be positive and less than total weight
		        for (int n = 1; n <= items; n++) {
		            value[n] = (int) (Math.random() * 1000);
		            weight[n] = (int) (Math.random() * capacity1);
		        }
			    
				long startRecursive = System.currentTimeMillis();
				int rresult = fill(items, capacity1, capacity2);
				long stopRecursive = System.currentTimeMillis();
				System.out.println("recursive result: " + rresult);
				System.out.println("recursive time: " + (stopRecursive - startRecursive));
				
				long startDynamic = System.currentTimeMillis();
				int dresult = dynamic();
				long stopDynamic = System.currentTimeMillis();
				System.out.println("dyamic result: " + dresult);
				System.out.println("dynamic time: " + (stopDynamic - startDynamic));

				out.write(i + "\t" + (stopRecursive - startRecursive) + "\t" + (stopDynamic - startDynamic) + "\n");

			}

			out.close();
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    private static int fill(int i, int w1, int w2) throws Exception {

    	sleep(1);
    	
    	// base cases
    	if(i == 0) {
    		return 0;
    	}
    	
    	if(w1 < 0 || w2 < 0) {
    		return Integer.MIN_VALUE;
    	}

    	if(cache[i][w1][w2] != 0) {
    		return cache[i][w1][w2];
    	}
    	else {
	    	int option1 = fill(i - 1, w1, w2);
	    	int option2 = fill(i - 1, w1 - weight[i], w2) + value[i];
	    	int option3 = fill(i - 1, w1, w2 - weight[i]) + value[i];
	    	
	    	int max = Math.max( option1, Math.max(option2, option3) );
	    	cache[i][w1][w2] = max;
			return max;
    	}
    }

    
    private static int dynamic() throws Exception {

    	sleep(1);
    	
		int[][][] options = new int[items + 1][capacity1 + 1][capacity2 + 1];
		boolean[][][] solutions = new boolean[items + 1][capacity1 + 1][capacity2 + 1];

		// create all the base cases (bottom up for dynamic programming
		// solution)
		for (int n = 1; n <= items; n++) {

			for (int w1 = 1; w1 <= capacity1; w1++) {

				for (int w2 = 1; w2 <= capacity2; w2++) {

					// get the last option
					int option1 = options[n - 1][w1][w2];

					int option2 = 0;
					if (weight[n] <= w1) {
						option2 = options[n - 1][w1 - weight[n]][w2] + value[n];
					}

					int option3 = 0;
					if (weight[n] <= w2) {
						option3 = options[n - 1][w1][w2 - weight[n]] + value[n];
					}

					// select better of two options
					options[n][w1][w2] = Math.max(option1, Math.max(option2, option3));
					solutions[n][w1][w2] = option1 < Math.max(option2, option3);
				}
			}
		}
		
        int result = 0;
		for (int n = 1; n <= items; n++) {
			for (int w1 = 1; w1 <= capacity1; w1++) {
				for (int w2 = 1; w2 <= capacity2; w2++) {
					if(solutions[n][w1][w2]) {
						result += options[n][w1][w2];
					}
				}
			}
		}

		return result;
    }
}
