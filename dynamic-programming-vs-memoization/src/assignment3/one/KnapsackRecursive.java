package assignment3.one;

public class KnapsackRecursive {

	private static int itemCount = 5;
    private static int capacity = 10;

    private static int[] value = new int[itemCount + 1];
    private static int[] weight = new int[itemCount + 1];

    public static void main(String[] args) {

		//    item	value	weight	take
		//    1		603		0		true
		//    2		339		5		false
		//    3		693		6		true
		//    4		667		8		false
		//    5		179		6		false
        value[1] = 603;		weight[1] = 0;
        value[2] = 339;		weight[2] = 5;
        value[3] = 693;		weight[3] = 6;
        value[4] = 667;		weight[4] = 8;
        value[5] = 179;		weight[5] = 6;
        
		long start = System.currentTimeMillis();

		int result = fill(itemCount, capacity);
		
		System.out.println(result);
		
        long stop = System.currentTimeMillis();

        System.out.println("Time: " + (start - stop));
    }
    
    private static int fill(int i, int w) {
    	
    	// base cases
    	if(i == 0) {
    		return 0;
    	}
    	
    	if(w == 0) {
    		return 0;
    	}

    	// item is too big
    	if(weight[i] > w) {
    		return fill(i - 1, w);
    	}
    	else {
    		return Math.max( fill(i - 1, w), fill(i - 1, w - weight[i]) + value[i]);
    	}
    }
}
