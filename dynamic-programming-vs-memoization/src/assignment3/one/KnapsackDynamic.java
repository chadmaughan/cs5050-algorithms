package assignment3.one;

public class KnapsackDynamic {

    public static void main(String[] args) {

    	int itemCount = 5;
        int capacity = 10;

        int[] value = new int[itemCount + 1];
        int[] weight = new int[itemCount + 1];

        // generate random values and weights for items, item weight should be positive and less than total weight
        for (int n = 1; n <= itemCount; n++) {
            value[n] = (int) (Math.random() * 1000);
            weight[n] = (int) (Math.random() * capacity);
        }

		long startSimple = System.currentTimeMillis();

        // opt[n][w] = max value of packing items 1..n with weight limit w
        // sol[n][w] = does opt solution to pack items 1..n with weight limit w include item n?
        int[][] opt = new int[itemCount + 1][capacity + 1];
        boolean[][] sol = new boolean[itemCount + 1][capacity + 1];

        int[][] opt1 = new int[itemCount + 1][capacity + 1];
        int[][] opt2 = new int[itemCount + 1][capacity + 1];

        // create all the base cases (bottom up for dynamic programming solution)
        for (int n = 1; n <= itemCount; n++) {

        	System.out.println("n: " + n);
        	
            for (int w = 1; w <= capacity; w++) {

            	System.out.println("w: " + w);

            	// get the last option
                int option1 = opt[n-1][w];
                opt1[n][w] = option1;
                
                System.out.println("Option 1: " + option1);

                // 
                int option2 = 0;
                if (weight[n] <= w) {
                	option2 = opt[n-1][w-weight[n]] + value[n];
                    System.out.println("Option 2: " + option2 + " (n-1: " + (n-1) + "w: " + w + " weight[n]: " + weight[n] + " value[n]: " + value[n]);
                    
                }
                opt2[n][w] = option2;

                // select better of two options
                opt[n][w] = Math.max(option1, option2);
                sol[n][w] = (option2 > option1);
            }
        }

        // determine which items to take
        boolean[] take = new boolean[itemCount + 1];
        for (int n = itemCount, w = capacity; n > 0; n--) {
        	
            if (sol[n][w]) { 
            	take[n] = true;  
            	w = w - weight[n]; 
            }
            else { 
            	take[n] = false;
            }
        }

        // print results
        System.out.println("item" + "\t" + "value" + "\t" + "weight" + "\t" + "take");
        for (int n = 1; n <= itemCount; n++) {
            System.out.println(n + "\t" + value[n] + "\t" + weight[n] + "\t" + take[n]);
        }

        long stopSimple = System.currentTimeMillis();
		System.out.println("simple: " + (stopSimple - startSimple));

		for(int y = 0; y < capacity; y++) {
			System.out.print("\t" + y);
		}
		System.out.print("\n");

		for(int x = 0; x < itemCount; x++) {
			System.out.println("x: " + x);
			for(int y = 0; y < capacity; y++) {
				System.out.print("\t" + opt1[x][y]);
			}
			System.out.print("\n");
			
			for(int y = 0; y < capacity; y++) {
				System.out.print("\t" + opt2[x][y]);
			}
			System.out.print("\n");

			for(int y = 0; y < capacity; y++) {
				System.out.print("\t" + sol[x][y]);
			}
			System.out.print("\n");

			for(int y = 0; y < capacity; y++) {
				System.out.print("\t" + opt[x][y]);
			}
			System.out.print("\n");

		}
		
		System.out.println("");
		
    }
}
