package assignment3.one;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class KnapsackCombined extends Thread {

	private static int items = 1000;
    private static int capacity = 1;	// set below, slowly increases

    private static int[] value = new int[items + 1];
    private static int[] weight = new int[items + 1];

    private static int[][] cache = new int[items + 1][capacity + 1];

    public static void main(String[] args) {

		try {

			FileWriter fstream = new FileWriter("/tmp/data-1000.txt");
			BufferedWriter out = new BufferedWriter(fstream);

			for (int i = 2; i <= 8; i += 2) {

				System.out.println("i: " + i);
				
			    capacity = i;

			    value = new int[items + 1];
			    weight = new int[items + 1];

			    cache = new int[items + 1][capacity + 1];

		        // generate random values and weights for items, item weight should be positive and less than total weight
		        for (int n = 1; n <= items; n++) {
		            value[n] = (int) (Math.random() * 1000);
		            weight[n] = (int) (Math.random() * capacity);
		        }
			    
				long startRecursive = System.currentTimeMillis();
				int rresult = fill(items, capacity);
				long stopRecursive = System.currentTimeMillis();
				System.out.println("recursive result: " + rresult);
				System.out.println("recursive time: " + (stopRecursive - startRecursive));
				
				long startDynamic = System.currentTimeMillis();
				int dresult = dynamic();
				long stopDynamic = System.currentTimeMillis();
				System.out.println("dyamic result: " + dresult);
				System.out.println("dynamic time: " + (stopDynamic - startDynamic));

				out.write(i + "\t" + (stopRecursive - startRecursive) + "\t" + (stopDynamic - startDynamic) + "\n");

			}

			out.close();

		}
		catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    private static int dynamic() throws Exception {

    	sleep(1);
    	
        int[][] options = new int[items + 1][capacity + 1];
        boolean[][] solutions = new boolean[items + 1][capacity + 1];

        // create all the base cases (bottom up for dynamic programming solution)
        for (int n = 1; n <= items; n++) {

            for (int w = 1; w <= capacity; w++) {

            	// get the last option
                int option1 = options[n-1][w];
                
                int option2 = 0;
                if (weight[n] <= w) {
                	option2 = options[n-1][w-weight[n]] + value[n];
                }

                // select better of two options
                options[n][w] = Math.max(option1, option2);
                solutions[n][w] = (option2 > option1);
            }
        }

        // determine which items to take
        int result = 0;
        for (int n = items, w = capacity; n > 0; n--) {
        	
            if (solutions[n][w]) { 
            	result += value[n];
            	w = w - weight[n]; 
            }
        }

        return result;
    }

    private static int fill(int i, int w) throws Exception {

    	sleep(1);
    	
    	if(cache[i][w] == 0) {
    		
	    	// base cases
	    	if(i == 0) {
	   			cache[i][w] = 0;
	    		return 0;
	    	}
	    	
	    	if(w == 0) {
	   			cache[i][w] = 0;
	    		return 0;
	    	}
	
	    	// item is too big
	    	if(weight[i] > w) {
	    		
	    		int result = fill(i - 1, w);
	   			cache[i][w] = result;
	    		return result;
	    	}
	    	else {
	    		int option1 = fill(i - 1, w);    		
	    		int option2 = fill(i - 1, w - weight[i]) + value[i];
	
	    		int result = Math.max( option1, option2 );
	   			cache[i][w] = result;
	
	    		return result;
	    	}
    	}
    	else {
    		return cache[i][w];
    	}
    }
}
