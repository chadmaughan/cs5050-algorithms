﻿using System;
using System.Numeric;
using System.Collections;
using System.Collections.Generic;

public class GraphComplexity
{
    private int n; // size
    private int[,] graph; // connectivity graph

    public GraphComplexity(double probability, int size)
	{
        // generates a random graph
        n = size;
        graph = new int[n, n];
        generateRandomGraph(probability);
	}
    public GraphComplexity(int degree, int size)
    {
        // returns a random graph whose average degree is degree
        n = size;
        graph = new int[n, n];
        generateAverageDegreeGraph(degree);
    }
    public void complementGraph()
    {
        // changes the graph to be its complement
        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                if (graph[i, j] == 1)
                {
                    graph[i, j] = 0;
                    graph[j, i] = 0;
                }
                else
                {
                    graph[i, j] = 1;
                    graph[j, i] = 1;
                }
    }
    public bool isConnected(int i, int j)
    {
        if (i >= 0 && j>= 0 && i < n && j < n)
            return graph[i, j] == 1;
        return false;
    }
    public void setConnected(int i, int j)
    {
        graph[i, j] = 1;
        graph[j, i] = 1;
    }
    public double averageDegree()
    {
        int sum = 0;
        for(int i = 0; i < n; i++)
            for(int j = i+1; j < n; j++)
                sum += graph[i, j];
        return (double)2 * sum / (double)n;
    }
    public double computeComplexity()
    {
        // takes a graph represented as an integer connectivity matrix (symetric) with graph[i,j] == 1 means an edge, == 0 no edge
        // and returns a double that represents the complexity of the graph (higher the most information contained in the graph)
        // assumes that the graph has no self connections
        int[,] c_i = new int[n, 2]; // the count of connections to a single node. the second index is not to i ,0] or connections to i ,1]
        double[,] p_i = new double[n, 2]; //as above but probabilities
        int[, , ,] c_ij = new int[n, n, 2, 2]; //count of connections between two nodes through another node, last two indexes similar to above
        double[, , ,] p_ij = new double[n, n, 2, 2]; // as above but probabilities
        // fill in p_i by looking at connectivity between each i and all the other nodes
        for (int connect = 0; connect < 2; connect++)
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    if (i != j && graph[i, j] == connect)
                        c_i[i, connect]++;
        // fill in c_ij by looking at connectivity between i and j through all other nodes
        for (int connect_i = 0; connect_i < 2; connect_i++)
            for (int connect_j = 0; connect_j < 2; connect_j++)
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                        for (int m = 0; m < n; m++) // middle node
                            if (i != j && i != m && j != m && graph[i, m] == connect_i && graph[j, m] == connect_j)
                                c_ij[i, j, connect_i, connect_j]++;
        // convert to probabilities by dividing by the total number of possibilities 
        //(taking into account that each node must be distinct)
        for (int connect = 0; connect < 2; connect++)
            for (int i = 0; i < n; i++)
                p_i[i, connect] = c_i[i, connect] / (double)n;
        // convert to probabilities by dividing by the total
        for (int connect_i = 0; connect_i < 2; connect_i++)
            for (int connect_j = 0; connect_j < 2; connect_j++)
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                        p_ij[i, j, connect_i, connect_j] = c_ij[i, j, connect_i, connect_j] / (double)(n - 2);
        // compute the shannon entropy
        double[] k = new double[n];
        for (int connect = 0; connect < 2; connect++)
            for (int i = 0; i < n; i++)
                if (p_i[i, connect] != 0)
                    k[i] += -1 * p_i[i, connect] * Math.Log(p_i[i, connect], 2);
        // compute the mutual distance d[]
        double[,] d = new double[n, n];
        // initialize to 1
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                d[i, j] = 1.0;
        // for each connection subtract the mutual information
        for (int connect_i = 0; connect_i < 2; connect_i++)
            for (int connect_j = 0; connect_j < 2; connect_j++)
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                        if (p_ij[i, j, connect_i, connect_j] != 0 && p_i[i, connect_i] != 0 && p_i[j, connect_j] != 0)
                            d[i, j] += -1 * p_ij[i, j, connect_i, connect_j] * Math.Log(p_ij[i, j, connect_i, connect_j] / (p_i[i, connect_i] * p_i[j, connect_j]), 2);
        // accumulate the answer over all n
        double psi = 0.0;
        double f = 0.0, sumD = 0.0;
        for (int i = 0; i < n; i++)
        {
            sumD = 0.0;
            for (int j = 0; j < n; j++)
                sumD = +d[i, j] * (1 - d[i, j]);
            f = (2 / (double)(n * (n - 1))) * sumD;
            psi += k[i] * f;
        }
        return psi;
    }
 //Private methods
    private void generateRandomGraph(double probability)
    {
        // p is the probability of a connection between i and j
        // returns a 2D int connectivity graph with n nodes
        Random ran = new Random();
        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                if (ran.NextDouble() <= probability)
                {
                    graph[i, j] = 1;
                    graph[j, i] = 1;
                }
    }
    private class Edge
    {
        public int from;
        public int to;

        public Edge(int f, int t)
        {
            from = f;
            to = t;
        }
    }
    private void generateAverageDegreeGraph(int degree)
    {
        // returns a 2D int connectivity graph with n nodes
        // where the average degree of each node is degree and connections are random
        if (degree >= n)
            return; // fail but no exception handling.
        int count = Convert.ToInt16(degree * n / 2);
        List<Edge> edges = new List<Edge>();
        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                edges.Add(new Edge(i, j));
        int total = edges.Count;
        Random ran = new Random();
        for (int e = 0; e < count; e++)
        {
            int choice = ran.Next(total);
            Edge oneE = edges[choice];
            edges.RemoveAt(choice);
            total--;
            graph[oneE.to, oneE.from] = 1;
            graph[oneE.from, oneE.to] = 1;
        }
    }
}
