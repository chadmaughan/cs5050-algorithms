package gcbb;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;


public class GraphColorability {

	private int[] vcolor;

	private int colors = 3;

	private int size = 100;
	
	private int[][] graph;

	private boolean exit = false;
	
	public static void main(String[] args) {
		new GraphColorability();
	}
	
	public GraphColorability() {
	
		try {

			FileWriter fstream = new FileWriter("/tmp/graph-coloring-data.txt");
			BufferedWriter out = new BufferedWriter(fstream);

			DecimalFormat df = new DecimalFormat("#.##########");

			double complexity = 0.1;
			for(int i = 0; i < 1000; i++) {
	
				System.out.println("i: " + i);
				if(i % 100 == 0 && i > 0) {
					complexity += .1d;
					System.out.println("New complexity: " + complexity);
				}

				long rStart = System.currentTimeMillis();

				this.vcolor = new int[size];
				for(int z = 0; z < size; z++) {
					vcolor[z] = -1;
				}
				
				GraphComplexity gc = new GraphComplexity(complexity, size);
				this.graph = gc.getGraph();

				double psi = gc.computeComplexity();

				//printGraph(graph);
				boolean initial = color(0);
				System.out.println("initial: " + initial);

				long rStop = System.currentTimeMillis();
				
				String output = i + "\t" + df.format(psi) + "\t" + (rStop - rStart) + System.getProperty("line.separator");

				System.out.println(output);
				out.write(output);
			}
			
			out.close();

		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public String getColor(int c) {
		if(c == 0) {
			return "blue";
		}
		else if(c == 1) {
			return "green";
		}
		else if(c == 2) {
			return "red";
		}
		else {
			return "black";
		}
	}
	
	public boolean valid(int[] vcolor) {
		boolean result = true;
		for(int i = 1; i < vcolor.length; i++) {
			if(vcolor[i-1] == vcolor[i]) {
				System.out.println("vcolor[" + (i-1) + "] (" + vcolor[i-1] + ") != vcolor[" + i + "] (" + vcolor[i] + ")");
				result = false;
			}
		}
		return result;
	}

	public boolean color(int i) {
				
		System.out.println("Node: " + i);
		
		if(!exit) {
			if(i == size) {
				System.out.println("success");
				for(int v = 0; v < vcolor.length; v++) {
					System.out.println("v: " + v + " - " + getColor(vcolor[v]));
				}
				
				// only get one solution (even though others may exist)
				exit = true;
				
				return exit;
			}
			
			// get all the colors of the neighbors
			Set<Integer> usedColors = new HashSet<Integer>();
			for(int j = 0; j < graph[i].length; j++) {
	
				// a node can't have an edge to itself
				if(i != j) {
	
					// is the node a neighbor (does it have an edge connecting them)?
					if(graph[i][j] == 1) {
	
						// does node j have an assigned color?
						if(vcolor[j] >= 0) {
							usedColors.add(vcolor[j]);
						}
					}
				}
			}
	
			// check for failure
			if(usedColors.size() < colors) {
				for(int color = 0; color < colors; color++) {
					
					// don't try a color a neighbor node uses
					if(!usedColors.contains(color)) {
						System.out.println("trying: " + getColor(color) + " (" + i + ")");
						vcolor[i] = color;
						boolean rr = color(i+1);
						System.out.println("recursive result: " + rr);
					}
					else {
						System.out.println("Skipping color: " + getColor(color));
					}
				}
				
				// unassign a color
				System.out.println("unassign: " + i);
				vcolor[i] = -1;
				return exit;
			}
			else {
				return false;
			}
		}
		
		System.out.println("last: " + exit);
		return exit;
	}
	
	public void printGraph(int[][] costMatrix) {

		StringBuffer matrix = new StringBuffer();
		matrix.append(System.getProperty("line.separator"));

		StringBuffer header = new StringBuffer("      ");
		StringBuffer separator = new StringBuffer("     -");
		for (int i = 0; i < costMatrix.length; i++) {
			header.append(String.format("%1$#3s", i));
			separator.append("---");
		}

		matrix.append(header);
		matrix.append(System.getProperty("line.separator"));
		matrix.append(separator);
		matrix.append(System.getProperty("line.separator"));

		StringBuffer rows = new StringBuffer();
		for (int i = 0; i < costMatrix.length; i++) {
			rows.append(String.format("%1$#3s", i));
			rows.append(" | ");
			for (int j = 0; j < costMatrix[i].length; j++) {
				if (i != j) {
					rows.append(String.format("%1$#3s", costMatrix[i][j]));
				}
				else {
					rows.append("  -");
				}
			}
			rows.append(System.getProperty("line.separator"));
		}

		matrix.append(rows);
		System.out.println(matrix);
	}
}
